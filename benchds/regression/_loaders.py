import os
import numpy as np
import pandas as pd


def __fetch_data__(name: str, header=None) -> pd.DataFrame:
    pathname = os.path.abspath(os.path.dirname(__file__))
    data = pd.read_csv(f"{pathname}/files/{name}_full.dat",
                       header=header,
                       sep=" ")
    data.columns = [f"X{i}" for i in range(1, data.shape[1] + 1)]
    return data


def airfoil() -> pd.DataFrame:
    """
    Load the Airfoil Self-Noise dataset.
    The NASA data set comprises different size NACA 0012 airfoils at various wind tunnel speeds and angles of attack.
    The span of the airfoil and the observer position were the same in all experiments.
    The name of the columns are:
      1. Frequency, in Hertzs
      2. Angle of attack, in degrees
      3. Chord length, in meters
      4. Free-stream velocity, in meters per second
      5. Suction side displacement thickness, in meters
      6. Scaled sound pressure level, in decibels (output variable)
    See https://archive.ics.uci.edu/ml/datasets/airfoil+self-noise
    :return: a pd.DataFrame with the samples of the dataset
    """
    data = __fetch_data__("airfoil")
    data.columns = ["frequency", "angle_of_attack", "chord_length", "free_stream_velocity",
                    "suction_side_displacement_thickness", "scaled_sound_pressure_level"]
    return data


def boston() -> pd.DataFrame:
    """
    Load the Boston Housing Dataset

    The Boston Housing Dataset is a derived from information collected by the U.S. Census Service concerning housing
    in the area of Boston MA. The columns of the dataset are:

     1. CRIM - per capita crime rate by town
     2. ZN - proportion of residential land zoned for lots over 25,000 sq.ft.
     3. INDUS - proportion of non-retail business acres per town.
     4. CHAS - Charles River dummy variable (1 if tract bounds river; 0 otherwise)
     5. NOX - nitric oxides concentration (parts per 10 million)
     6. RM - average number of rooms per dwelling
     7. AGE - proportion of owner-occupied units built prior to 1940
     8. DIS - weighted distances to five Boston employment centres
     9. RAD - index of accessibility to radial highways
     10. TAX - full-value property-tax rate per $10,000
     11. PTRATIO - pupil-teacher ratio by town
     12. B - 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
     13. LSTAT - % lower status of the population
     14. MEDV - Median value of owner-occupied homes in $1000's (output variable)

    :return: a pd.DataFrame with the samples of the dataset
    """
    # from sklearn.datasets import load_boston
    # boston_ds = load_boston()
    #
    # data = pd.DataFrame(boston_ds.data,
    #                     columns=boston_ds.feature_names.tolist()).drop(columns=['CHAS'])
    # data['MEDV'] = boston_ds.target

    data = __fetch_data__("bostonhousing")
    data.columns = ["CRIM", "ZN", "INDUS", "CHAS", "NOX", "RM", "AGE", "DIS",
                    "RAD", "TAX", "PTRATIO", "B", "LSTAT", "MEDV"]
    return data


def california_housing() -> pd.DataFrame:
    """
    Load the California housing dataset
    :return:
    """
    from sklearn.datasets import fetch_california_housing
    data = fetch_california_housing(as_frame=True)
    return data.frame


def concrete() -> pd.DataFrame:
    """
    Load the Concrete Compressive Strength dataset.
    The columns of the dataset are:
      1. Name -- Data Type -- Measurement -- Description
      2. Cement (component 1) -- quantitative -- kg in a m3 mixture -- Input Variable
      3. Blast Furnace Slag (component 2) -- quantitative -- kg in a m3 mixture -- Input Variable
      4. Fly Ash (component 3) -- quantitative -- kg in a m3 mixture -- Input Variable
      5. Water (component 4) -- quantitative -- kg in a m3 mixture -- Input Variable
      6. Superplasticizer (component 5) -- quantitative -- kg in a m3 mixture -- Input Variable
      7. Coarse Aggregate (component 6) -- quantitative -- kg in a m3 mixture -- Input Variable
      8. Fine Aggregate (component 7) -- quantitative -- kg in a m3 mixture -- Input Variable
      9. Age -- quantitative -- Day (1~365) -- Input Variable
      10. Concrete compressive strength -- quantitative -- MPa -- Output Variable
      See https://archive.ics.uci.edu/ml/datasets/concrete+compressive+strength
    :return:
    """
    columns = ["cement_component_1", "blast_furnace_slag_component_2",
               "fly_ash_component_3", "water_component_4", "superplasticizer_component_5",
               "coarse_aggregate_component_6", "fine_aggregate_component_7",
               "age", "concrete_compressive_strength"]
    data = __fetch_data__("concrete")
    data.columns = columns
    return data


def dow_chemical() -> pd.DataFrame:
    data = __fetch_data__("dowchemical")
    return data


def energy_cooling() -> pd.DataFrame:
    """
    Load the Energy efficiency dataset.
    The dataset contains eight attributes (or features, denoted by X1...X8) and one response.
    The columns are:
      1. X1 Relative Compactness
      2. X2 Surface Area
      3. X3 Wall Area
      4. X4 Roof Area
      5. X5 Overall Height
      6. X6 Orientation
      7. X7 Glazing Area
      8. X8 Glazing Area Distribution
      9. y1 Heating Load
    See https://archive.ics.uci.edu/ml/datasets/energy+efficiency
    :return: a pd.DataFrame with 768 rows and 8 columns.
    """
    data = __fetch_data__("energycooling")
    data.columns = ["relative_compactness", "surface_area", "wall_area",
                    "roof_area", "overall_height", "orientation", "glazing_area",
                    "glazing_area_distribution", "heating_load"]
    return data


def energy_heating() -> pd.DataFrame:
    return __fetch_data__("energyheating")


def tower() -> pd.DataFrame:
    data = __fetch_data__("tower")
    return data


def wine_red() -> pd.DataFrame:
    return __fetch_data__("winered")


def wine_white() -> pd.DataFrame:
    return __fetch_data__("winewhite")


def yacht_hydrodynamics() -> pd.DataFrame:
    return __fetch_data__("yacht")


def nguyen(size: int, degree: int) -> pd.DataFrame:
    X = np.random.random(size)
    y = np.zeros(size)

    for i in np.arange(size):
        y[i] = np.sum([X[i] ** d for d in np.arange(1, degree + 1)])

    data = pd.DataFrame(data=X, columns=["X"])
    data['y'] = y

    return data


def hard(n: int, d: int) -> pd.DataFrame:
    X = np.random.randn(n, d)
    y = np.zeros(n)

    for i in range(n):
        y[i] = np.sum(np.fromiter((np.power(X[i:i + 1, j], j + 1) for j in np.arange(d)), dtype=float))

    data = pd.DataFrame(data=X, columns=[str(f"X{k + 1}") for k in range(d)])
    data['y'] = y

    return data


def polynomial(*kwargs) -> pd.DataFrame:
    X = 2 * np.random.randn(*kwargs)
    y = np.sum(X, axis=1)
    data = pd.DataFrame(data=X, columns=[str(f"X{i}") for i in range(1, np.shape(X)[1] + 1)])
    data["y"] = y
    return data
