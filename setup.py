#!/usr/bin/env python
from setuptools import setup, find_packages
import benchds

setup(
    name='benchds',
    version=benchds.__revision__,
    description='Benchmark regression for GP libraries',
    long_description=open('README.md').read(),
    author=benchds.__author__,
    author_email='msgp@inria.fr',
    url='https://gitlab.inria.fr/trust-ai/benchds',
    platforms=['any'],
    license='MIT',
    packages=find_packages(),
    package_data={'benchds.regression': ['files/**']},
    include_package_data=True,
    install_requires=[
        'pandas==1.4.2',
        'numpy==1.22.4',
        "scikit-learn==1.1.1"
    ],
)
