# GP Benchmark Datasets

## Origin

* Most of the datasets are taken from the [UCI Machine Learning](http://archive.ics.uci.edu/ml/index.php) repository
* Some datasets were taken from [here](https://drive.google.com/drive/folders/1cUU7f23z_lBPQCOX7h1unZWKZH3YO_EB) published by the paper[Solving the Exponential Growth of Symbolic Regression Trees in Geometric Semantic Genetic Programming](https://arxiv.org/pdf/1804.06808.pdf)
* Other datasets were found from links taken from old papers (do not remember).

## Dataset format

* Rows are examples, columns are features. 
* The last column is the label (classification) / target variable (regression). 
* Row values are separated by space.

## Missing values
* No missing values are present in the datasets, since the rows containing at least one missing value have been removed. 
* Find the original dataset (e.g., in the UCI repository) to retrieve the original data, with missing values.

## Normalization

* The datasets are not normalized
